DROP TABLE IF EXISTS "students";
CREATE TABLE "students"
 ( students TEXT, classId TEXT, classId2 TEXT, id INTEGER PRIMARY KEY );

\COPY "students" (id, students, classId, classId2) FROM 'Students.csv' WITH DELIMITER ',' CSV HEADER;

SELECT * FROM "students"
ORDER BY id;
