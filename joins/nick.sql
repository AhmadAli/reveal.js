DROP TABLE IF EXISTS nick;
CREATE TABLE nick (
  id SERIAL PRIMARY KEY,
  Nicks TEXT
);

\COPY "nick" (Nicks, id) FROM 'Nicknames.csv' WITH DELIMITER ',' CSV HEADER;

SELECT * FROM nick
ORDER BY id;
