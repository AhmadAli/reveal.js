# SQL Joins
### By Julian and Ahmad




# Nicknames.sql


```  [1|2|3|4|5]
DROP TABLE IF EXISTS Nick;
CREATE TABLE Nick (
  id SERIAL PRIMARY KEY,
  Nicks TEXT,
  Num TEXT
);
```



# Names.sql


``` [1|2|3|4|5|6]
 DROP TABLE IF EXISTS Names;
CREATE TABLE Names (
  id SERIAL PRIMARY KEY,
  First TEXT,
  Last TEXT,
  Num TEXT
); 
```


# Inner
``` [1|2|3|4]
SELECT *
FROM Names
INNER JOIN Nick
ON Names.num = Nick.num;
```

# Outer Full
``` [1|2|3|4]
 SELECT *
FROM Names
FULL OUTER JOIN Nick
ON Names.num = Nick.num '
```


# Left
``` [1|2|3|4|5|6]
DROP TABLE IF EXISTS nam;
CREATE TABLE IF EXISTS nam;
SELECT column_id
FROM Names
LEFT JOIN table2
ON Names.column_First = Nicknames.column_Nicks;
```
