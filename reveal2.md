# SQL Joins
### By Julian and Ahmad




# Nicknames.sql


' DROP TABLE IF EXISTS Nick;
CREATE TABLE Nick (
  id SERIAL PRIMARY KEY,
  Nicks TEXT,
  Num TEXT
); '



# Names.sql


' DROP TABLE IF EXISTS Names;
CREATE TABLE Names (
  id SERIAL PRIMARY KEY,
  First TEXT,
  Last TEXT,
  Num TEXT
); 
'


# Inner
'SELECT *
FROM Names
INNER JOIN Nick
ON Names.num = Nick.num'


# Outer Full
' SELECT *
FROM Names
FULL OUTER JOIN Nick
ON Names.num = Nick.num '


# Left
' DROP TABLE IF EXISTS nam;
CREATE TABLE IF EXISTS nam;
SELECT column_id
FROM Names
LEFT JOIN table2
ON Names.column_First = Nicknames.column_Nicks;'
