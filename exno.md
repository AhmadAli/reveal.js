# Exnoeration Registery 
### By Ahmad and Julian



# What is our project, why we chose it and where we found our data
For our project we decided to make an exnoeration registery. We chose this project because me and Julian are both inrested in the field of law to some capcity. We found our data on the university of michigans National exoneration registery.


# What is a it
An exoneration registery is a large database model of people who have been released from prison due to various reseans such as police miscunduct false charges and so on



## Python

``` [1|2|3|4|5|6|7|8|9|10|11|12|13|14|15|16|17|18|19|20|21|22|23|24|25|26|27|28]
import csv

# Open the input CSV file
with open('crime.csv', 'r') as input_file:
    reader = csv.reader(input_file)

    # Create a new CSV writer for the output file
    with open('output.csv', 'w', newline='') as output_file:
        writer = csv.writer(output_file)

        # Loop through each row in the input file
        for row in reader:
            # Split the row at the semicolon
            split_row = row[1].split(';')

            # Write the first part of the row to the output file
            output_row = [row[0], split_row[0]]

            # Write the remaining parts of the row to the output file as separate columns
            for value in split_row[1:]:
                output_row.append(value)

            # Pad the row with empty cells to ensure it has 12 columns
            while len(output_row) < 12:
                output_row.append('')

            # Write the row to the output file
            writer.writerow(output_row)
```



# Tag Identificaction 
Tag identification, or OM tags tell us why or how a person got exnoerated, below we have a table of OM tags and each was assigned their own unique number.


``` [1|2|3|4|5|6|7|8|9|10|11|12|13|14|15|16|17|18|19|20|21]
DROP TABLE IF EXISTS OM;
CREATE TABLE OM
 (omId SERIAL, Om TEXT, PRIMARY KEY (omId) );;

SELECT * FROM OM;

INSERT INTO OM
VALUES
(1,'PR'),
(2,'OF'),
(3,'FA'),
(4,'CW'),
(5,'WH'),
(6,'NW'),
(7,'KP'),
(8,'WT'),
(9,'INT'),
(10,'PJ'),
(11,'PL');

SELECT * From OM
```


# OM tag table 


``` [1|2|3|4|5|6|7|8|9]
DROP TABLE IF EXISTS "tags";
CREATE TABLE "tags"
 (tag1 SERIAL, id INTEGER PRIMARY KEY, tag2 SERIAL);

\COPY "tags" (id, tag1, tag2) FROM 'output.csv' WITH DELIMITER ',' CSV HEADER;

SELECT * FROM "tags"
ORDER BY id;
```

# The Final join


``` [1|2|3|4|5|6|7|8|9|10|11]
SELECT tags.id AS exonereeId, OM.omId AS reasonId
FROM tags
INNER JOIN OM ON tags.tag1 = Om.omId

UNION

SELECT tags.id AS exonereeId, OM.omId AS reasonId
FROM tags
INNER JOIN OM ON tags.tag2 = OM.omId

ORDER BY exonereeId ASC, reasonId ASC;
```


